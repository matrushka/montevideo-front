$(function() {
  var $apply = $('.regional-comparison-toolbar__item--submit');
  var $revert = $('.regional-comparison-toolbar__item--revert');
  var $help = $('.regional-comparison-toolbar__item--help');

  var $level = $('#regional-comparison-toolbar__compare-select');
  var $region = $('#regional-comparison-toolbar__region-select');

  // This is the container for the charts and indicators
  var $regional_comparison_data = $('.regional-comparison-data');

  $apply.click(function(e) {
    e.preventDefault();
    fetchRegionalComparison();
  });

  $revert.click(function(e) {
    e.preventDefault();
    $level.val("overall-overall").trigger('change');
    $region.val("region-ALL").trigger('change');
    $apply.click();
  });

  $help.click(function(e) {
    // console.log('clicked htlserk s');
    e.preventDefault();
    var $help_modal = $('.modal--regional-comparison-help').parent();
    $help_modal.fadeIn();
  });
  
  function fetchRegionalComparison() {
    var level_selection = $level.val();
    var region_selection = $region.val();
    
    var level_args = level_selection.split('-');
    var level = level_args[0];
    var level_id = level_args[1];

    var region_args = region_selection.split('-');
    var region_id = region_args[1];

    var partial_path = '/partials/' + lang + '/regional_comparison/' + level + '-' + region_id + '-' + level_id + '.html';
    var analytics_path = '/regional_comparison/' + level + '-' + region_id + '-' + level_id + '.html';
    
    $regional_comparison_data.fadeOut('fast');
    $.ajax({
      url: partial_path,
      success: function(data, status, jqXHR) {
        $regional_comparison_data.empty().append(data);
        setupIndicatorScroll();
        $regional_comparison_data.hide().fadeIn('slow');

        // Send a virtual page view to this regional comparison
        ga('send', 'pageview', analytics_path); 
      },
      error: function(jqXHR, status, error) {
        console.error('There was an error in getting the partial: ' + error);
      }
    });
  }

  function setupIndicatorScroll() {
    $(window).off('scroll');

    // When the window is scrolled, if it scrolls past the point where the indicators are
    // visible, reveal the floating indicators
    var $indicators = $('.indicators--static');
    var $floating_indicators = $('.indicators--floating');
    var indicators_height = $indicators.outerHeight();
    var indicators_offset = $indicators.offset();
    var start_showing_at = indicators_height + indicators_offset.top

    $(window).on('scroll', function(e) {
      if($(window).scrollTop() > start_showing_at) {
        $floating_indicators.addClass('is-visible');
      }
      else {
        $floating_indicators.removeClass('is-visible');
      }
    });
    $(window).scroll();
  }

  fetchRegionalComparison();
});
