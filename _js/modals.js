$(function() {
  // Show the modal__info_page when the home "more" button is clicked
  var $modals = $('.modal').parent();

  // Hide the modal__info_page when the X button is clicked
  $modals.find('.modal__close-link').click(function(e) {
    e.preventDefault();
    $(this).parents('.modal__wrapper').fadeOut();
  });
});
