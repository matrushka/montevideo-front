// Cosas que necesito uqe ocurran:
// Al elegir un tema, irnos a otra página con los mismos países, distinto tema
// 1. Al elegir un país, mostrarlo
// Al activar show answers y expand rows, activar las clases correspondientes

$(function() {
  // Initial Work
  // Define $table object and stretch it to an offset height apropiate value
  var $table = $('.country-comparison-table');
  var tableHeight = $(window).height() - $table.offset().top;
  tableHeight = (tableHeight > 500) ? tableHeight : 500;
  var $tableContainer = $table.parents(".country-comparison__table-container").first();
  $tableContainer.height(tableHeight);

  // TODO:
  // rebuildTable() Method

  // *****************************************
  // Update Clones definition and first call
  // *****************************************
  var updateClones = function() {
    // Table Header
    var $headerClone = $table.find(".country-comparison-table__header").clone(1);
    var $headerCloneReceptor = $(".country-comparison-table__clone--header");
    $headerCloneReceptor.empty().append($headerClone);

    // Table First Column
    var $questionsClone = $table.find(".country-comparison-table__body").clone(1);
    $questionsClone.find(".is-country-specific").remove();
    var $questionClonesReceptor = $(".country-comparison-table__clone--column");
    $questionClonesReceptor.empty().append($questionsClone);

    // Table first Cell
    var $cellClone = $headerClone.clone(1);
    $cellClone.find(".is-country-specific").remove();
    var $cellCloneReceptor = $(".country-comparison-table__clone--cell");
    $cellCloneReceptor.empty().append($cellClone);

    // Update column widths
    setTimeout(function() {
      var questionsCloneWidth = $table.find(".country-comparison-table__column--reactive").first().width();
      $questionClonesReceptor.width(questionsCloneWidth);
      $cellCloneReceptor.width(questionsCloneWidth);
      $(".country-comparison-table__clone-container-layer-2").height( $(".country-comparison-table__clone-container-layer-1").height() );
    }, 350);
  };

  // *****************************************
  // Grow height of question
  // *****************************************
  var stretchQuestions = function($questions) {
    $questions.each( function(index, question) {
      var $question = $(question);
      $questions.css('height', 'auto');
      var $row = $question.parents('.country-comparison-table__row');
      setTimeout(function() {
        $question.css('height', $row.outerHeight() + 'px');
      }, 50);
    });
  }

  // *****************************************
  // Watch for Scroll to show clone
  // *****************************************
  $tableContainer.on('scroll', function(e){
    var $columnCloneReceptor = $(".country-comparison-table__clone--column");
    var scrollValue = $tableContainer.scrollLeft();
    if( scrollValue > 40 ) {
      $columnCloneReceptor.css('opacity', '1');
    } else {
      $columnCloneReceptor.css('opacity', '0');
    }
  });

  // *****************************************
  // Show countries received in the hash
  // *****************************************
  var countryCodes = window.location.hash.slice(1).split('+')
  countryCodes.forEach(function(code) {
    var selector = '.is-country-specific.country-' + code;
    $(selector).addClass('is-visible');

    // TODO: improve setComparisonTableZebra to take visibility into account
    // setComparisonTableZebra();
    updateClones();
  });

  // *****************************************
  // Show a country when it is selected
  // *****************************************
  var $countriesSelect = $('#countries-select');
  $countriesSelect.on('change', function (e) {
    // Show the country column
    var countryCode = this.value
    if(countryCode != '') {
      // return the select component to its default value
      $(this).val('');

      // Save the country code in the URL hash
      var currentHash = window.location.hash.slice(1);
      var currentCountries = currentHash.split('+')
      
      // Search for Country in URL array
      var index = $.inArray(countryCode, currentCountries);

      if(index < 0) {
        // Update Clones
        updateClones();

        // Fade In selected country
        $('.country-' + countryCode).fadeIn();

        // Update current countries Hash
        currentCountries.push(countryCode);
        window.location.hash = currentCountries.join('+');

        // Update size of question containers
        setTimeout(function() {
          var $questions = $('.country-comparison-table__question');
          stretchQuestions($questions);
        }, 100);
      }
    }
  });

  // *****************************************
  // Remove a country from display
  // *****************************************
  $('.country-comparison-table__header-close-button').click(function (e) {
    var $questions = $('.country-comparison-table__question');

    var $button = $(e.currentTarget);
    var countryCode = $button.data('country');
    $('.country-' + countryCode).fadeOut();

    // Clear the country code from the URL hash
    var currentHash = window.location.hash.slice(1);
    var currentCountries = currentHash.split('+')
    var index = $.inArray(countryCode, currentCountries);
    currentCountries.splice(index,1);
    window.location.hash = currentCountries.join('+');

    stretchQuestions($questions);

    // Update Clones
    setTimeout(function() {
      updateClones();
    }, 500);
  });

  // *****************************************
  // Switch to a different subject
  // *****************************************
  var $subjectsSelect = $('#subjects-select');
  $subjectsSelect.on('change', function (e) {
    var subjectNid = this.value
    if(subjectNid != '') {
      var currentPath = window.location.pathname;
      var newPath = currentPath.replace(/[0-9]+\/$/, subjectNid + '/');

      window.location = newPath + window.location.hash
    }
  });

  // *****************************************
  // Enable show-answers checkbox
  // *****************************************
  $('#toggle-show-answers').change(function(e) {
    var isChecked = $(this).is(":checked");
    var $questions = $('.country-comparison-table__question');
    if(isChecked) {
      $table.addClass('is-showing-answers');
    }
    else {
      $table.removeClass('is-showing-answers');
      // Normalize height of question
      //$questions.css('height', 'auto');
    }

    stretchQuestions($questions);

    // Update Clones
    setTimeout(function() {
      updateClones();
    }, 100);
  });

  // *****************************************
  // Enable expand rows checkbox
  // *****************************************
  $('#toggle-expand-rows').change(function(e) {
    var isChecked = $(this).is(":checked");
    var $questions = $('.country-comparison-table__question');
    var $cloneTable = $('.country-comparison-table__clone');
    if(isChecked) {
      $table.addClass('is-showing-expanded-rows');
      $cloneTable.addClass('is-showing-expanded-rows');
    }
    else {
      $table.removeClass('is-showing-expanded-rows');
      $cloneTable.removeClass('is-showing-expanded-rows');
      // Normalize height of question
      // $questions.css('height', 'auto');
    }

    stretchQuestions($questions);

    // Update Clones
    setTimeout(function() {
      updateClones();
    }, 100);
  });
  
  // *****************************************
  // Toggle is-expanded class on row, on click
  // *****************************************
  $('.country-comparison-table__row').click(function(e) {
    // Exit row state toggle if show answers is not checked
    if( $('#toggle-show-answers').is(":checked") ) return false;

    // Exit row state toggle if no-trigger class is found
    var $row = $(e.currentTarget);
    if( $row.hasClass('is-no-trigger') ) return false;
    
    // Continue Row Expansion
    $row.toggleClass('is-expanded');
    var $question = $row.find('.country-comparison-table__question');
    if( $row.hasClass('is-expanded')) {
      $question.css('height', $row.outerHeight() + 'px');
    } else {
      $question.css('height', 'auto');
    }
    
    // stretchQuestions($questions);

    // Update Clones
    setTimeout(function() {
      updateClones();
    }, 100);    
  });
})
