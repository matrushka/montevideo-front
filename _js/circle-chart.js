paper.install(window);

$(function() {
  var $charts = $('.circle-graph');
  $charts.each(function() {
    var $chartContainer = $(this);
    var chartId = 'chart-' + $chartContainer.data('subtopic_id');
    $chartContainer.append("<canvas class='topic-period-data-card__heading-circle-graph__circle-graph ' id='" + chartId + "'></canvas>");
    paper.setup(chartId);

    var progress = $chartContainer.data('progress');
    var paperChart = new CircleChart(progress, {});
    paperChart.draw();
  });
});

function CircleChart(progress, options) {
  var progress = progress;

  var defaultOptions = {
    radius: 23,
    chartSize: 60,
    strokeWidth: 4,
    chartBackgroundColor: '#F0F0F0',
  }

  //Make a clone of the default config to avoid modifying it
  this.config = JSON.parse(JSON.stringify(defaultOptions));
  
  // Copy the received options to the object config
  for(var prop in this.config) {
    if(typeof( options[prop] ) != 'undefined') {
      this.config[prop] = options[prop];
    }
  }

  this.draw = function() {
    var initialAngle = -90;
    var endAngle = this.getProgressDegrees() - 90;
    var throughAngle = (this.getProgressDegrees() / 2) - 90;


    // Draw the background circle
    var background = new Path.Circle(view.center, this.config.radius);
    background.fillColor = this.config.chartBackgroundColor;

    // Get the points of the arc, by their angles
    var initialPoint = this.getPointByAngle(initialAngle);
    var throughPoint = this.getPointByAngle(throughAngle);
    var endPoint = this.getPointByAngle(endAngle);

    // var ci = new Path.Circle(initialPoint, 2);
    // var ct = new Path.Circle(throughPoint, 2);
    // var ce = new Path.Circle(endPoint, 2);
    // ci.fillColor = 'red';
    // ct.fillColor = 'red';
    // ce.fillColor = 'red';

    var arc = new Path.Arc(initialPoint, throughPoint, endPoint);
    arc.strokeWidth = this.config.strokeWidth;
    arc.strokeColor = this.getColorByProgress();
    arc.strokeCap = 'round';
  }

  this.getProgressDegrees = function() {
    return Math.min(359.9, (360 * (progress / 100)));
  }

  this.getPointByAngle = function(angle) {
    var angleRadians = angle * (Math.PI / 180);
    var x = this.config.radius * Math.cos(angleRadians);
    var y = this.config.radius * Math.sin(angleRadians);
    return new Point(view.center.x + x, view.center.y + y);
  }

  this.getColorByProgress = function() {
    if(progress <= 40) {
      return '#F44336';
    }
    else if (progress > 40 && progress <= 55) {
      return '#FF9100';
    }
    else if (progress > 55 && progress <= 70) {
      return '#FFEE58';
    }
    else if (progress > 70 && progress <= 90) {
      return '#4CAF50';
    }
    else if (progress > 90) {
      return '#2E7D32';
    }
  }
}
