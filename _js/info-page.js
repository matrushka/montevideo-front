$(function() {
  // Show the modal__info_page when the home "more" button is clicked
  var $info_page_wrapper = $('.modal--info-page').parent();

  $('.home-banner__action-button').click(function(e) {
    e.preventDefault();
    $info_page_wrapper.fadeIn();

    ga('send', 'pageview', '/modal--about-us');
  });
});
