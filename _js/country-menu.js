$(function() {
  $country_menu_wrapper = $('.modal--country-menu').parent();

  $('.main-menu__link--countries').click(function(e) {
    e.preventDefault();
    $country_menu_wrapper.fadeIn();
  });

  $country_menu_wrapper.find('.modal__close-link').click(function(e) {
    e.preventDefault();
    $country_menu_wrapper.fadeOut();
  });
});
