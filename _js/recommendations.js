$(function() {
  var $recommendation_buttons = $('.country-summary-topic__auxiliar-text');
  var $recommendation_blocks = $('.modal--recommendations .subject-recommendations');

  $recommendation_buttons.click(function(e) {
    var $button = $(this);
    var nid = $button.data('subject_nid');

    // Hide all the recommendations
    $recommendation_blocks.hide();

    // Show the block that corresponds to the clicked NID
    $recommendation_blocks.filter('.subject-recommendations--subject-nid-' + nid).show();

    // Fade the recommendations modal in
    $('.modal--recommendations').parent().fadeIn();
  });
});
