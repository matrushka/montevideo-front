$(function() {

  var $charts = $('.secrat-chart__chart');

  $charts.each(function(chart) {
    var $chart = $(this);
    var config_id = $chart.data('chart_config_id');
    var chart_config = window[config_id];

    $chart.append('<canvas id="'+config_id+'"></canvas>');

    new Chart(document.getElementById(config_id), chart_config);

  });
});
