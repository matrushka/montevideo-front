require_relative 'EvaluationDetailsModule.rb'
require_relative 'GeneratorEnabler.rb'
require_relative 'PageAnalytics.rb'

module Jekyll
  class CountryComparisonPage < Page
    include Jekyll::EvaluationDetails
    include Jekyll::PageAnalytics

    def initialize(site, base, dir, lang, subject_group_id, subject)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang
      @subject_group_id = subject_group_id
      @subject_nid = subject['id']

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'page.html');

      self.data['lang'] = lang
      self.data['default_language'] = site.config['default_language']
      self.data['type'] = 'country_comparison'
      self.data['title'] = 'Comparación de países'
      self.data['comparison_data'] = getComparisonData
      # pp getComparisonData
      # abort
      self.data['subjects'] = getSubjectsHash
      self.data['subjects_sorted'] = self.data['subjects'].values.sort! { |a,b| a['name'] <=> b['name'] }
      self.data['subject'] = self.data['subjects'][@subject_nid]
      self.data['countries'] = site.data["countries#{@lang}"]

      add_analytics_data
    end

    def getComparisonData
      comparison_data = {}

      countries = site.data["countries#{@lang}"]
      countries.each do |country|

        @country_code = country['code']
        self.data['comparison_chart_data'] = getComparisonChartData

        comparison_data[country['code']] = {
          "code" => country['code'],
          "name" => country['name'],
          "evaluation_details" => getEvaluationDetails(@subject_nid, site.config['current_evaluation_period'])
        }
      end

      comparison_data
    end
  end

  class CountryComparisonGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      puts "** Generating Subject comparison pages"

      # Iterate over languages
      site.config['languages'].each do |lang|

        # Iterate over subject groups (notice how the iteration
        # does not start with countries this time)
        content_tree = site.data['content_tree']
        subject_groups = content_tree['subject_groups']
        subject_groups.each do |sgid, subject_group|
          subjects = subject_group['subjects']

          # Iterate over subjects
          subjects.each do |subject_nid, subject|
            if lang != site.config['default_language']
              dir = "#{lang}/compare/#{subject_nid}"
            else
              dir = "compare/#{subject_nid}"
            end
            site.pages << CountryComparisonPage.new(site, site.source, dir, lang, sgid, subject)
          end

        end
      end
    end
  end
end
