require 'pp'
require 'json'
require 'csv'
require 'open-uri'
require_relative 'TranslationFilter.rb'
require_relative 'GeneratorEnabler.rb'
require_relative 'PageAnalytics.rb'
require_relative 'SubmissionTablesModule.rb'

module Jekyll
  class SubjectDetailsPage < Page
    include Jekyll::EvaluationDetails
    include Jekyll::EvalColor
    include Jekyll::TranslationFilter
    include Jekyll::PageAnalytics
    include Jekyll::SubmissionTables

    def initialize(site, base, dir, lang, country, subject_group_id, subject)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang
      @country_code = country['code']
      @country_name = country['name']
      @subject_group_id = subject_group_id
      @subject_nid = subject['id']
      @subjects = getSubjectsHash
      addSubjectEvaluation

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'page.html');

      self.data['lang'] = lang
      self.data['default_language'] = site.config['default_language']
      self.data['type'] = 'subject_details'
      self.data['title'] = nil
      self.data['title_prefix'] = "#{@subjects[@subject_nid]['name']}"
      self.data['title_sufix'] = " - #{@country_name}"
      self.data['country_code'] = @country_code
      self.data['country_name'] = @country_name
      
      self.data['header_chart_data'] = @subjects
      self.data['subject'] = @subjects[@subject_nid]
      self.data['comparison_chart_data'] = getComparisonChartData
      self.data['evaluation_periods'] = getEvaluationPeriods
      self.data['evaluation_details'] = getEvaluationDetails(@subject_nid, site.config['current_evaluation_period'])

      self.data['secrat'] = site.data['secrat']["#{@country_code}#{@lang}"]['age_groups']
      self.data['secrat_chart_data'] = getSecratChartData

      self.data['tables'] = getSubmissionTables(@country_code, @subject_nid)
      self.data['documents'] = getSubmissionDocuments(@country_code, @subject_nid)
      self.data['priority_actions'] = getPriorityActions

      add_analytics_data
    end

    # In the future, this should get a period and load the documents
    # only for the current period. The documents data should be period-suffixed
    # or otherwise marked by period
    def getSubmissionDocuments(country_code, subject_nid)
      submission_documents_id = "country_#{country_code}_subject_#{subject_nid}"
      submission_documents = site.data['submission_documents'][submission_documents_id]

      documents = []

      submission_documents.each do |nid, doc|
        documents.push( {
          "name" => doc['name'],
          "file_url" => doc['file'].sub('public://', site.config['files_base_url'])
        } )
      end

      documents.sort! { |a,b| a['name'].downcase <=> b['name'].downcase }

      documents
    end

    def getPriorityActions
      priority_actions_data = site.data["priority_actions#{@lang}"]
      subject_priority_actions = priority_actions_data.select { |action| action['subject_nid'] == @subject_nid }
      subject_priority_actions
    end

    def getSecratChartData
      secrat_data = site.data['secrat']["#{@country_code}#{@lang}"]['age_groups']
      chart_data = {}

      secrat_data.each do |nid, age_group|
        chart_data[nid] = {
          'name' => age_group['name'],
          'learning_focuses' => getSecratLearningFocusesChartData(age_group),
          'key_concepts' => getSecratKeyConceptsChartData(age_group),
        }
      end

      chart_data
    end

    def getSecratLearningFocusesChartData(age_group)
      chart_data = {}

      possible_answers = {
        "si" => 'Rasgos fuertes',
        "no" => 'Rasgos débiles',
        "masomenos" => 'Rasgos intermedios',
        "" => 'Sin respuesta'
      }

      labels = []
      datasets = {} # will convert to array later
      age_group['learning_focuses_evaluation'].each do |lfid, learning_focus|
        counts = learning_focus['count']

        counts.each do |answer_id, count|
          next unless answer_id != 'components_total'

          # If the dataset for the answer hasn't been initialized,
          # create an empty one, with placeholder for data, its label and 
          # background color that corresponds to the answer

          if datasets[answer_id].nil?

            # Translate the answer if the page is not in the default language
            # This is usually done at the template level, but it would be more complex to 
            # alter the JSON data in Liquid than it is to translate here.
            if @lang != site.config['default_language']
              label = t(possible_answers[answer_id], @lang)
            else
              label = possible_answers[answer_id]
            end

            color_code = case answer_id
            when 'si'
              'dark-green'
            when 'masomenos'
              'orange'
            when 'no'
              'red'
            else
              'grey'
            end

            datasets[answer_id] = {
              'label' => label,
              'data' => [],
              'backgroundColor' => get_color_values[color_code]
            }
          end

          datasets[answer_id]['data'].push(count)
        end

        labels.push(learning_focus['label'])
      end
      
      chart_data = {
        "labels" => labels,
        "datasets" => datasets.values
      }

      chart = {
        "type" => "horizontalBar",
        "data" => chart_data,
        "options" => {
          "plugins" => {
            "stacked100" => {'enable' => 'true'}
          }
        }
      }

      chart
    end

    def getSecratKeyConceptsChartData(age_group)
      # necesitamos obtener:
      # labels
      # un solo dataset, con "data" (valores para cada label)
      # un solo dataset, con "backgroundColor" (colores para cada label)
      # un solo dataset con "label" (dice percent)
      labels = []
      data = []
      backgroundColor = '#5CC4FF'

      age_group['key_concepts_evaluation']['details'].each do |nid, subtopic|
        labels.push(subtopic['label'])
        data.push(subtopic['percent'].round)
        # backgroundColor.push(eval_color_value(subtopic['percent']))
      end

      chart_data = {
        "labels" => labels,
        "datasets" => [{
          "label" => t("Porcentaje", @lang),
          "data" => data,
          "backgroundColor" => backgroundColor
        }]
      }

      chart = {
        "type" => "horizontalBar",
        "data" => chart_data,
        "options" => {
          "scales" => {
            "xAxes" => [{
              "ticks" => {
                "beginAtZero" => true,
                "max"=> 100
              }
            }]
          }
        }
      }

      chart
    end

  end

  class SubjectDetailsGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      puts "** Generating Subject Details pages"

      # Iterate over languages
      site.config['languages'].each do |lang|

        # Iterate over countries
        countries = site.data["countries#{lang}"]
        countries.each do |country|

          content_tree = site.data['content_tree']
          subject_groups = content_tree['subject_groups']

          # Iterate over subject groups
          subject_groups.each do |sgid, subject_group|
            subjects = subject_group['subjects']
            
            # Iterate over subjects
            subjects.each do |subject_nid, subject|
              if lang != site.config['default_language']
                dir = "#{lang}/countries/#{country['code']}/#{subject_nid}"
              else
                dir = "countries/#{country['code']}/#{subject_nid}"
              end
              site.pages << SubjectDetailsPage.new(site, site.source, dir, lang, country, sgid, subject)
            end
          end

          # Countries loop ends here.
          # Uncomment the break to stop at ARG:
          # break;
        end
      end
    end
  end
end
