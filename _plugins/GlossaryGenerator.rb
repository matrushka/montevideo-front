require 'pp'
require_relative 'GeneratorEnabler.rb'
require_relative 'PageAnalytics.rb'

module Jekyll
  class GlossaryPage < Page
    include Jekyll::PageAnalytics

    def initialize(site, base, dir, lang)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'page.html')
      self.data['lang'] = lang
      self.data['default_language'] = site.config['default_language']

      self.data['title'] = "Glosario"
      self.data['type'] = 'glossary'
      self.data['glossary'] = site.data["glossary#{lang}"]

      add_analytics_data
    end
  end

  class GlossaryGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      site.config['languages'].each do |lang|
        puts "** Generating Glossary page for language #{lang}"

        if lang != site.config['default_language']
          dir = "#{lang}/glossary/"
        else
          dir = "glossary"
        end

        site.pages << GlossaryPage.new(site, site.source, dir, lang)
      end
    end
  end
end
