require 'pp'
require_relative 'GeneratorEnabler.rb'
require_relative 'PageAnalytics.rb'

module Jekyll
  class HomePage < Page
    include Jekyll::PageAnalytics

    def initialize(site, base, dir, lang)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'page.html')
      self.data['lang'] = lang
      self.data['default_language'] = site.config['default_language']

      self.data['title'] = 'Inicio'
      self.data['title_sufix'] = ' - Mira que te Miro'
      self.data['type'] = 'home'

      add_analytics_data
    end
  end

  class HomePageGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      site.config["languages"].each do |lang|
        puts "** Generating Homepage for language #{lang}"

        if lang != site.config['default_language']
          dir = "#{lang}"
        else
          dir = ""
        end

        site.pages << HomePage.new(site, site.source, dir, lang)
      end
    end
  end
end
