module Jekyll
  module LocalizePathFilter
    def LocalizePathFilter.init defaultLanguage
      @defaultLanguage = defaultLanguage
    end

    def LocalizePathFilter.defaultLanguage
      @defaultLanguage
    end

    def lp(path, language)
      if (language == LocalizePathFilter.defaultLanguage)
        return path
      else
        return "/#{language}#{path}"
      end
    end

    def lswitch(path, currentLanguage, targetLanguage)
      defaultLanguage = LocalizePathFilter.defaultLanguage
      
      # If target is the same as source language, just return the path unmodified
      if (targetLanguage == currentLanguage) 
        return path

      # If target is the default language, strip the language prefix
      # (we know by now that target isn't the same as source)
      elsif (targetLanguage == defaultLanguage)
        return path[3..-1]

      # If target is not the default, and not the source, append the language prefix
      else
        return "/#{targetLanguage}#{path}"
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::LocalizePathFilter)
