module Jekyll
  module EvalColor
    def eval_color(progress)
      case progress
        when 0..40
          'red'
        when 41..55
          'orange'
        when 56..70
          'yellow'
        when 71..90
          'light-green'
        when 91..100
          'dark-green'
      end
    end

    def eval_color_value(progress)
      color_values = get_color_values 
      color_values[eval_color(progress)]
    end

    def get_color_values
      {
        'red' => '#F44336',
        'orange' => '#FF9100',
        'yellow' => '#FFEE58',
        'light-green' => '#2E7D32',
        'dark-green' => '#4CAF50',
        'grey' => '#BEBEBE'
      }
    end
  end
end

Liquid::Template.register_filter(Jekyll::EvalColor)
