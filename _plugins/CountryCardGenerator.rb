require 'pp'
require_relative 'GeneratorEnabler.rb'
require_relative 'PageAnalytics.rb'

module Jekyll
  class CountryCardPage < Page
    include Jekyll::PageAnalytics

    def initialize(site, base, dir, lang, country)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang
      @country_code = country['code']
      @country_name = country['name']

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'page.html')
      self.data['lang'] = lang
      self.data['default_language'] = site.config['default_language']
      self.data['type'] = 'country_card'
      self.data['title'] = 'Tarjeta de país'
      self.data['title_prefix'] = "#{@country_name} - "
      self.data['country_code'] = country['code']
      self.data['country_name'] = country['name']
      self.data['country_text'] = getCountryText
      self.data['country_progress'] = getCountryTotalPoints
      self.data['country_progress_negative'] = 100 - getCountryTotalPoints
      self.data['chart_data'] = getCountryChartData

      add_analytics_data
    end

    def getCountryText
      text_entry = site.data["country_text#{@lang}"].select { |ct| ct['country_code'] == @country_code }

      if(text_entry.length == 0)
        return ''
      else 
        return text_entry[0]['text']
      end
    end

    def getCountryTotalPoints
      site.data['evaluation_subject_group']
        .select { |sg| sg['country_code'] == @country_code }
        .map { |sg| sg['points'] }
        .reduce { |sum, sg| sum + sg }
        .round
    end

    def getCountryChartData
      chart_data = []
      site.data['content_tree']['subject_groups'].each do |sgid, subject_group|
        chart_data.push(getSubjectGroupChartData(subject_group))
      end

      chart_data
    end

    def getSubjectGroupChartData(subject_group)
      sg = {
        "id" => subject_group['id'],
        "name" => subject_group['name'][@lang],
        "progress" => getSubjectGroupProgress(subject_group),
        "progress_negative" => 100 - getSubjectGroupProgress(subject_group),
        "bars" => getSubjectGroupBars(subject_group)
      }

      sg
    end

    def getSubjectGroupProgress(subject_group)
      sgid = subject_group['id']

      # Get the sum of points in all the subjects
      points_awarded = site.data['evaluation_subject']
        .select { |s| s['subject_group'] == sgid && s['country_code'] == @country_code }
        .map { |s| s['points'] }
        .reduce { |sum, points| sum+points }

      # Get the max points of all the subjects
      max_points = site.data['content_tree']['subject_groups'][sgid]['subjects']
        .map { |sid, s| s['points_weight'] }
        .reduce { |sum, p| sum + p }

      # Calculate the percent progress
      (points_awarded / max_points * 100).round
    end

    def getSubjectGroupBars(subject_group)
      sgid = subject_group['id']
      group_subjects_evaluation = site.data['evaluation_subject'].select { |s| s['subject_group'] == sgid && s['country_code'] == @country_code }
      country_submission_tables = site.data['submission_tables'][@country_code]['subjects']

      bars = []
      site.data['content_tree']['subject_groups'][sgid]['subjects'].each do |sid, s|
        subject_evaluation = group_subjects_evaluation.select { |s| s['subject'] == sid }[0]
        subject_recommendations = country_submission_tables[sid]['recommendations'].reject { |k, r| r['text'].nil? }
        subject_recommendations = (subject_recommendations.size == 0) ? false : subject_recommendations

        bars.push({
          "name" => s['name'][@lang],
          "subject_nid" => sid,
          "progress" => subject_evaluation['percentage'].round,
          "progress_negative" => 100 - subject_evaluation['percentage'].round,
          "recommendations" => subject_recommendations,
        })
      end
      
      bars
    end
  end

  class CountryCardGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      puts "** Generating Country pages"
      site.config['languages'].each do |lang|

        countries = site.data["countries#{lang}"]
        countries.each do |country|

          if lang != site.config['default_language']
            dir = "#{lang}/countries/#{country['code']}"
          else
            dir = "countries/#{country['code']}"
          end

          site.pages << CountryCardPage.new(site, site.source, dir, lang, country)
        end
      end
    end
  end
end
