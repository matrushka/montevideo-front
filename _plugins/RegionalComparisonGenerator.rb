require 'pp'
require_relative 'GeneratorEnabler.rb'

module Jekyll
  class RegionalComparisonGenerator < Generator
    include Jekyll::GeneratorEnabler

    def generate(site)
      return unless is_enabled? site

      generateForSubjectGroups(site)
      # generateForSubjectGroups is actually building ALL the pages at the global points,
      # subject group and subject levels. While writing it, I probably found that 
      # since the whole content tree was already being traversed, it made no sense
      # to traverse it tons of times, each for a different level.
      # These functions will not be needed:
      #
      # generateForSubjects(site)
      # generateForPoints(site)
    end

    def generateForSubjectGroups(site)
      # To disable the regional comparison generator
      # uncomment this return:
      # return

      site.config["languages"].each do |lang|
        dir = "partials/#{lang}/regional_comparison"

        # Generate a page for the special case, ALL countries, GLOBAL points
        site.pages << RegionalComparison.new(site, site.source, dir, lang, 'overall', 'overall', 'ALL')

        # Generate a page for each of the regions
        site.data['regions' + lang]['regions'].each do |region_id, region|
          site.pages << RegionalComparison.new(site, site.source, dir, lang, 'overall', 'overall', region_id)
        end

        # ******************************************************
        # Iterate over the SUBJECT GROUPS, generate a page
        # for each of them, and all the regions, plus ALL
        # ******************************************************
        site.data['content_tree']['subject_groups'].each do |subject_group_id, subject_group|

          # Generate the page for the special case, ALL countries
          site.pages << RegionalComparison.new(site, site.source, dir, lang, 'subject_group', subject_group_id, 'ALL')

          # Generate a page for each of the regions
          site.data['regions' + lang]['regions'].each do |region_id, region|
            site.pages << RegionalComparison.new(site, site.source, dir, lang, 'subject_group', subject_group_id, region_id)
          end

          # ******************************************************
          # Iterate over the SUBJECTS of the subject_group,
          # generate a page for each of them, and all the regions
          # ******************************************************
          #
          # Generate the page for the special case, ALL countries
          subject_group['subjects'].each do |subject_id, subject|

            # Generate the page for the special case, ALL countries
            site.pages << RegionalComparison.new(site, site.source, dir, lang, 'subject', subject_id, 'ALL')

            # Generate a page for each of the regions
            site.data['regions' + lang]['regions'].each do |region_id, region|
              site.pages << RegionalComparison.new(site, site.source, dir, lang, 'subject', subject_id, region_id)
            end
          end
        end
      end
    end

    def generateForSubjects(site)
    end

    def generateForPoints(site)
    end
  end

  class RegionalComparison < Page
    def initialize(site, base, dir, lang, level, level_id, region_id)
      @site = site
      @base = base
      @dir = dir
      @name = "#{level}-#{region_id}-#{level_id}.html"
      @lang = lang

      layout = "partials/regional_comparison"
      self.read_yaml(File.join(base, '_layouts'), "#{layout}.html")

      data_method = "get_#{level}_data"
      if self.respond_to?(data_method)
        self.data['chart_data'] =  self.public_send(data_method, level_id, region_id)
      end

      self.process(@name)

      self.data['lang'] = lang
      self.data['layout'] = layout
      self.data['level_id'] = level_id
      self.data['region_id'] = region_id
      
    end

    def get_overall_data(level_id, region_id)
      countries_list = get_region_countries region_id

      # Get the subject_group IDs
      subject_group_ids = site.data['content_tree']['subject_groups'].keys

      # Avoid the content_tree clutter, produce a Hash of subject_groups, keyed
      # by id, each with fill_class and name
      subject_groups = Hash[subject_group_ids.map { |v| [v, {
        'fill_class' => "fill-color--#{v}",
        'name' => site.data['content_tree']['subject_groups'][v]['name'][@lang],
        'sum' => 0,
        'average' => 0
      }]}]
      
      # Make access to evaluation data easier
      evaluation = site.data['evaluation_subject_group']
      
      countries = []
      countries_list.each do |source_country|
        country = source_country.clone
        country['bars'] = {}
        country_progress = 0
        country_evaluation = evaluation.select { |v| v['country_code'] == country['code'] }

        subject_groups.each do |sgid, sg|
          sg_evaluation = country_evaluation.select { |v| v['subject_group'] == sgid }.last

          # Add the progress of the country to calculate the average later
          sg['sum'] += sg_evaluation["percentage"]
          
          country['bars'][sgid] = {
            "fill_class" => sg['fill_class'],
            "progress" => sg_evaluation['percentage'],
            "progress_negative" => 100 - sg_evaluation["percentage"]
          }
          country_progress += sg_evaluation['points']
        end

        country['progress'] = country_progress.round
        country['progress_negative'] = 100 - country_progress.round
        countries.push(country)
      end

      # Calculate the average total country progress for the region
      country_average = (countries.map { |c| c['progress'] }.reduce(:+) / countries.length).round
      country_average_negative = 100 - country_average

      # Calculate the average for each subject_group
      subject_groups.each do |sgid, sg|
        sg['average'] = (sg['sum'] / countries.length).round
        sg['average_negative'] = 100 - sg['average']
      end

      {
        "countries" => countries,
        "country_average" => country_average,
        "country_average_negative" => country_average_negative,
        "categories" => subject_groups,
      }
    end

    def get_subject_group_data(level_id, region_id)
      countries_list = get_region_countries region_id

      subjects_data = site.data['content_tree']['subject_groups'][level_id]['subjects']
      subject_nids = subjects_data.keys

      subjects = Hash[subject_nids.map { |v| [v, {
        'fill_class' => "fill-color--subject-nid-#{v}",
        'name' => subjects_data[v]['name'][@lang],
        'points_weight' => subjects_data[v]['points_weight'],
        'sum' => 0,
        'average' => 0
      }]}]

      evaluation = site.data['evaluation_subject']

      countries = []
      countries_list.each do |source_country|
        country = source_country.clone
        country['bars'] = {}
        country_progress = 0
        country_evaluation = evaluation.select { |v| v['country_code'] == country['code'] }

        # These values will be accumulated while looping
        # through the subjects, then used to calculate
        # the average progress percent for the country
        # in this particular subject_group
        country_max_points = 0
        country_total_points = 0

        subjects.each do |sid, s|
          s_evaluation = country_evaluation.select { |v| v['subject'] == sid }.last

          # Add the progress of the country to calculate the average later
          s['sum'] += s_evaluation['points']

          country['bars'][sid] = {
            "fill_class" => s['fill_class'],
            "progress" => s_evaluation['percentage'].round,
            "progress_negative" => 100 - s_evaluation['percentage'],
            "points" => s_evaluation['points']
          }

          country_max_points += subjects[sid]['points_weight']
          country_total_points += s_evaluation['points']
        end

        country['progress'] = (country_total_points / country_max_points * 100).round
        country['progress_negative'] = 100 - country['progress']

        countries.push(country)
      end

      subjects.each do |sid, s|
        #               ( the average # of points   ) / the max points     * turn to percent).round 
        s['average'] = ((s['sum'] / countries.length) / s['points_weight'] * 100).round
        s['average_negative'] = 100 - s['average']
      end

      # Calculate the average total country progress for the region
      country_average = (countries.map { |c| c['progress'] }.reduce(:+) / countries.length).round
      country_average_negative = 100 - country_average

      {
        "countries" => countries,
        "country_average" => country_average,
        "country_average_negative" => country_average_negative,
        "categories" => subjects,
      }
    end

    def get_subject_data(level_id, region_id)
      subject_nid = level_id
      countries_list = get_region_countries region_id

      # TODO: review these 4 lines, are they still useful, or just an old experiment?
      subjects_data = {}
      site.data['content_tree']['subject_groups'].each do |sgid, subject_group|
        subjects_data.merge!(subject_group['subjects'])
      end

      # Just one subject! much easier :)
      subject = {
        'fill_class' => "fill-color--subject-nid-#{subject_nid}",
        'name' => subjects_data[subject_nid]['name'][@lang],
        # TODO: remove sum from hash? It doesn't seem to be used later, so the value remains 0,
        # which may confuse other devs working with the data
        'sum' => 0,
        'average' => 0,
        'points_weight' => subjects_data[subject_nid]['points_weight']
      }
      subjects = {subject_nid => subject}

      evaluation = site.data['evaluation_subject']
      subject_evaluation = evaluation.select { |v| v['subject'] == subject_nid }

      countries = []
      countries_list.each do |source_country|
        country = source_country.clone
        country_evaluation = subject_evaluation.select { |v| v['country_code'] == country['code'] } 
        country['bars'] = {}
        s_evaluation = country_evaluation.last
        country['bars'][subject_nid] = {
          'fill_class' => subject['fill_class'],
          'progress' => s_evaluation['percentage'].round,
          'progress_negative' => 100 - s_evaluation['percentage'].round,
          'points' => s_evaluation['points']
        }
        countries.push(country)
      end
      
      average_progress = (countries.map { |c| c['bars'][subject_nid]['progress']}.reduce(:+) / countries.length).round
      subject['average'] = average_progress
      subject['average_negative'] = 100 - average_progress

      {
        "countries" => countries,
        "categories" => subjects
      }
    end

    def get_region_countries(region_id)
      all_countries = site.data["countries#{@lang}"]

      if(region_id == 'ALL')
        return all_countries
      end

      region = site.data["regions#{@lang}"]["regions"]["#{region_id}"]
      region_countries = all_countries.select { |c| region['countries'].include? c['code'] }

      region_countries
    end
  end
end
