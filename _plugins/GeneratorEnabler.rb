require 'pp'

module Jekyll
  module GeneratorEnabler
    def is_enabled?(site)
      # If ALL is set to true, return true
      if site.config['generators']['ALL'] === true
        return true
      end

      my_class = self.class.to_s.split('::').last
      setting = site.config['generators'][my_class]
      
      # If the generator is not explicitly enabled in configuration 
      # (.nil?), keep it enabled by default
      is_enabled = setting.nil? || setting

      if(!is_enabled)
        puts "** Skipping generator #{my_class}"
      end

      return is_enabled
    end
  end
end
