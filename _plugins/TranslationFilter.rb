module Jekyll
  module TranslationFilter
    def TranslationFilter.init translation_catalog, defaultLanguage
      @catalog = translation_catalog
      @defaultLanguage = defaultLanguage
      $missing_translations = {}
    end

    def TranslationFilter.catalog
      @catalog
    end

    def TranslationFilter.defaultLanguage
      @defaultLanguage
    end

    def t(translatable, language)
      if (language == TranslationFilter.defaultLanguage)
        return translatable
      end

      translation = TranslationFilter.catalog[translatable]
      if translation.nil?
        $missing_translations[translatable] = true
        return translatable
      end
      
      translation[language]
    end

    def TranslationFilter.report
      puts "MISSING TRANSLATIONS: "

      $missing_translations.each do |missing, v|
        puts ">> \"#{missing}\""
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::TranslationFilter)

Jekyll::Hooks.register :site, :post_write do |site|
  Jekyll::TranslationFilter.report
end
