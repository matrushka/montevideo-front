require 'pp'

Jekyll::Hooks.register :site, :after_init do |site|
  puts ">> Preparing site data"

  Montevideo.installAssets(%w(img fonts js lib css));

  # Merge front JS with markup JS
  FileUtils.cp_r Dir.glob('_js/*.js'), 'js'

  # Merge front lib with markup lib
  FileUtils.cp_r Dir.glob('_lib/*.js'), 'lib'

  if (File.exist? '_data')
    FileUtils.rm_rf '_data'
  end
  FileUtils.cp_r 'data', '_data'
  
  # Copy translations to _data
  FileUtils.cp_r Dir.glob('_translation/*.csv'), '_data'
end

Jekyll::Hooks.register :site, :post_read do |site|
  default_language = site.config['default_language']

  # Prepare the translation catalog
  translation_catalog = {};
  site.data['t'].each do |row|
    translation_catalog[row[default_language]] = row.reject { |k,v| k == default_language }
  end

  Jekyll::TranslationFilter.init(translation_catalog, default_language)
  Jekyll::LocalizePathFilter.init(default_language)


  # Prepare the pages for easier use
  Montevideo.preparePages(site)
  Montevideo.prepareCountries(site)
  Montevideo.prepareSubjectGroups(site)
  Montevideo.sortGlossary(site)
end

module Montevideo
  def self.installAssets(dirs)
    if dirs.is_a? String
      dirs = [dirs]
    end

    dirs.each do |dir|
      if (File.exist? dir) 
        FileUtils.rm_rf dir
      end

      FileUtils.cp_r "_style_guide/#{dir}", dir
    end
  end

  # Access page info like this:
  # site.data['pages'][page.lang]['textohome']['body']
  def self.preparePages(site)
    puts ">> Preparing pages for access by code"

    pages = {}
    site.config['languages'].each do |lang|
      pages[lang] = {}
      site.data['pages' + lang].each do |page|
        pages[lang][page['code']] = {
          "title" => page['title'],
          "body" => Montevideo.insertPageImages(page['body'])
        }
      end
    end

    site.data['pages'] = pages
  end

  def self.insertPageImages(body)
    # Search for the pattern [img_responsive:IMG_ID] and replace it with an <img> tag
    body.gsub(/\[(img-[a-z\-]+):([a-zA-Z_0-9.]+)\]/, '<img src="/content_files/pages/\2" class="\1">')
  end

  # Access countries info like this:
  # site.data['countries'][page.lang]['ARG']['name']
  def self.prepareCountries(site)
    puts ">> Preparing countries for easy access"

    countries = {}
    site.config['languages'].each do |lang|
      countries[lang] = {}
      countries_source = site.data['countries' + lang].sort_by! { |c| c['name'] }

      countries_source.each do |country|
        countries[lang][country['code']] = {
          "code" => country['code'],
          "name" => country['name']
        }
      end
    end

    site.data['countries'] = countries
  end

  def self.prepareSubjectGroups(site)
    # Rename the subject group keys in the content tree
    # substitute spaces with underscores
    # lowercase the whole thing
    subject_groups = site.data['content_tree']['subject_groups'] 
    site.data['content_tree']['subject_groups'] = (subject_groups.keys.map { |k| k.gsub(/ /, '_').downcase }.zip subject_groups.values).to_h
    subject_groups = site.data['content_tree']['subject_groups'] 
    
    # Rename the IDs to match the keys, 
    subject_groups.each do |id ,subject_group|
      subject_group['id'] = id
    end

    # Perform the same substitution in the subject_group IDs
    # for all evaluation levels
    sg_evaluation = site.data['evaluation_subject_group']
    sg_evaluation.each do |sg|
      sg['subject_group'] = sg['subject_group'].gsub(/ /, '_').downcase
    end

    s_evaluation = site.data['evaluation_subject']
    s_evaluation.each do |s|
      s['subject_group'] = s['subject_group'].gsub(/ /, '_').downcase
    end

    st_evaluation = site.data['evaluation_subtopic']
    st_evaluation.each do |st|
      st['subject_group'] = st['subject_group'].gsub(/ /, '_').downcase
    end
  end

  def self.sortGlossary(site)
    site.config['languages'].each do |lang|
      site.data["glossary#{lang}"].sort! { |a,b| a['name'].downcase <=> b['name'].downcase }
    end
  end
end
