module Jekyll
  module SubmissionTables

    # In the future, this should get a period and load the tables / info
    # only for the current period. The files would be period-suffixed or 
    # otherwise marked by period
    def getSubmissionTables(country_code, subject_nid)
      tables_data = site.data['submission_tables'][country_code]['subjects'][subject_nid]
      sections = ['recommendations', 'details']

      sections.each do |section|
        tables_data[section].each do |nid, item|
          if !item['file'].nil?
            item['file'].sub!('public://', site.config['files_base_url'])
            file_uri = URI.escape(item['file'])
            file_contents = open(file_uri, "r:UTF-8", &:read)
            item['table_data'] = CSV.parse(file_contents)
          end
        end
      end

      tables_data
    end

  end
end
