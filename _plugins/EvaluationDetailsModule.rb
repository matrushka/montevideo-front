module Jekyll
  module EvaluationDetails
    
    # Generates a hash of subtopics, keyed by +subtopic_nid+, and
    # each element with:
    #
    # * *id*: The subtopic nid
    # * *name*: The localized name of the subtopic
    # * *points_weight*: The number of points the subtopic provides to the subject it belongs to
    # * *subject_nid*: The subject this subtopic belongs to
    #
    # *Requires*
    #
    # * @+lang+: The current language code
    # * +site.data+:
    def getSubtopicsHash
      subtopics = {}

      subject_groups = site.data['content_tree']['subject_groups']

      subject_groups.each do |sgid, subject_group|
        subject_group['subjects'].each do |subject_nid, subject|
          subject['subtopics'].each do |subtopic_nid, subtopic|
            subtopics[subtopic_nid] = {
              "id" => subtopic_nid,
              "name" => subtopic['name'][@lang].split('-')[1].lstrip,
              "points_weight" => subtopic['points_weight'],
              "subject_nid" => subject_nid
            }
          end
        end
      end

      subtopics
    end
    
    # Generates a hash of subtopics, keyed by subtopic NID, each with:
    # * +progress+
    # * +progress_negative+
    # * +components+: The result of +getSubtopicComponents+
    #
    # *Requires*
    # * +self.data ['comparison_chart_data']+, containing the result of +getComparisonChartData+ for the current subject
    # * Being able to run +getSubtopicComponents+
    def getEvaluationDetails(subject_nid, period)
      subtopics = getSubtopicsHash.select { |sid, subtopic| subtopic['subject_nid'] == subject_nid }

      subtopics.each do |sid, subtopic|
        subtopic['progress'] = self.data['comparison_chart_data'][sid]['progress'][period]
        if subtopic['progress'].nil?
          pp subtopic
          pp self.data['comparison_chart_data']
        end
        subtopic['progress_negative'] = 100 - subtopic['progress']
        subtopic['components'] = getSubtopicComponents(sid, period)
      end

      subtopics
    end

    # Generates a hash of components that belong to a subtopic.
    #
    # *Requires*
    # * @+subject_group_id+
    # * @+subject_nid+
    # * Being able to run +getComponent+
    def getSubtopicComponents(subtopic_nid, period)
      # Build a hash of the components, with their form key and localized name, and child components
      components = {}
      tree_components = site.data['content_tree']['subject_groups'][@subject_group_id]['subjects'][@subject_nid]['subtopics'][subtopic_nid]['components']

      tree_components.each do |form_key, component|
        components[form_key] = getComponent(tree_components, form_key)
      end

      components
    end
    
    # Returns a hash with the basic info of the component and
    # any child components, recursively
    #
    # *Requires*:
    # * Being able to run +getComponentDocuments+
    def getComponent(components_hash, form_key)
      component_data = components_hash[form_key]
      component = {
        'type' => component_data['type'],
        'id' => component_data['id'],
        'name' => component_data['name'][@lang],
        'answer' => getComponentAnswer(component_data),
        'color' => getComponentEvaluationColor(component_data),
        'child_components' => {}
      }

      unless !component_data['child_components']
        component_data['child_components'].each do |child_form_key, child_component|
          component['child_components'][child_form_key] = getComponent(component_data['child_components'], child_form_key)
        end
      end

      component['documents'] = getComponentDocuments(form_key)

      component
    end
    
    # Returns either a string (for textarea and textfield components)
    # or an array (for select components), with the answer(s) given
    #
    # *Requires*:
    # * @+country_code+
    # * @+subject_nid+
    # * +site.data+
    # * @+lang+
    def getComponentAnswer(component_data)
      submission_details_id = "country_#{@country_code}_subject_#{@subject_nid}"
      submission_details = site.data['submission_details'][submission_details_id]
      component_type = component_data['type']
      form_key = component_data['id']
      component_answer = submission_details[form_key]['value']

      if component_type == 'markup'
        return ''
      elsif component_type == 'select'
        answer = []
        checked_options = component_data['options'].select { |option| component_answer && (component_answer.include? option['id']) }
        checked_options.each do |option|
          answer.push(option['name'][@lang])
        end
        return answer
      else
        return component_answer
      end
    end

    # Returns the evaluation color for a component
    #
    # *Requires*
    # * @+country_code+
    # * @+subject_nid+
    def getComponentEvaluationColor(component_data)
      submission_details_id = "country_#{@country_code}_subject_#{@subject_nid}"
      submission_details = site.data['submission_details'][submission_details_id]
      form_key = component_data['id']

      submission_details[form_key]['color']
    end

    # Generates the list of documents that belong to a component.
    # Returns an array of components in which each item is a hash with:
    # * +name+: The name of the document.
    # * +file+: The document file path.
    # * +notes+: The notes for the document in the component entry.
    #
    # *Requires*:
    # @+country_code+
    # @+subject_nid+
    # +site.data+
    def getComponentDocuments(form_key)
      submission_documents_id = "country_#{@country_code}_subject_#{@subject_nid}"
      submission_documents = site.data['submission_documents'][submission_documents_id]
      documents = submission_documents.select { |document_nid, document| document['notes'].keys.include? form_key } 

      component_documents = []
      documents.each do |doc_nid, doc|
        component_documents.push({
          "name" => doc['name'],
          "file" => doc['file'].sub('public://', site.config['files_base_url']),
          "notes" => doc['notes'][form_key]
        })
      end
      component_documents
    end

    # Generates a hash of subjects based on the content tree. The hash
    # is keyed by subject_nid, and each element has:
    # * +subject_group_id+
    # * +id+
    # * +name+: the localized name
    # * +points_weight+: The number of points the weight provides in its subject_group
    #
    # *Requires*:
    # * @+lang+
    def getSubjectsHash
      subjects = {}

      subject_groups = site.data['content_tree']['subject_groups']

      subject_groups.each do |sgid, subject_group|
        subject_group['subjects'].each do |subject_nid, subject|
          subjects[subject_nid] = {
            "subject_group_id" => sgid,
            "id" => subject_nid,
            "name" => subject['name'][@lang],
            "points_weight" => subject['points_weight'],
          }
        end
      end

      subjects
    end

    # Adds evaluation data to the existing @+subjects+ hash.
    # After running this method, each subject in @+subjects+ will have +progress+ and +progress_negative+ values.
    #
    # *Requires*:
    # * @+country_code+: The current country code
    # * @+subjects+: The subjects hash, as produced by +getSubjectsHash+
    def addSubjectEvaluation
      country_subject_evaluation = site.data['evaluation_subject'].select { |item| item['country_code'] == @country_code }

      @subjects.each do |subject_nid, subject_data|
        subject_evaluation = country_subject_evaluation.select { |s| s['subject'] == subject_nid }[0]
        @subjects[subject_nid]['progress'] = subject_evaluation['percentage'].round
        @subjects[subject_nid]['progress_negative'] = 100 - subject_evaluation['percentage'].round
      end
    end
    
    # The periods don't do much currently, this is just a 
    # preparation for the moment multiple periods are actually
    # added to the platform
    #
    # *Returns*:
    # An array of evaluation periods (Strings) defined in the configuration
    def getEvaluationPeriods
      return site.config['evaluation_periods']
    end

    # Generates the data for the subtopics comparison chart shown in the Subject Details page. 
    #
    # The structure returned is a hash keyed by subtopic NID, each item with:
    # * +id+: the NID
    # * +name+: The localized name
    # * +progress+: A hash keyed by period ID (ie 2017), each containing the progress percentage for that period.
    #
    # *Requires*:
    #
    # * @+subject_nid+
    # * Being able to run +getSubtopicsHash+
    # * +site.config+
    # * +site.data+
    # * @+subject_nid+
    # * @+country_code+
    def getComparisonChartData
      subtopics = getSubtopicsHash
      subject_subtopics = subtopics.select { |sid, subtopic| subtopic['subject_nid'] == @subject_nid }

      # Build the bars hash, each with its name and id;
      # evaluation for multiple periods will be added later
      bars = {}
      subject_subtopics.each do |sid, subtopic|
        bar = {
          "id" => sid,
          "name" => subtopics[sid]["name"],
          "progress" => {}, # This hash will hold the evaluation progress for multiple periods
        }

        bars[sid] = bar
      end

      site.config['evaluation_periods'].each do |period|
        # In the future, when multiple periods are added, this line
        # should have a period suffix. See the montevideo-front Wiki
        subtopic_evaluation = site.data['evaluation_subtopic']
        subject_subtopics_evaluation = subtopic_evaluation.select { |item| 
          item['subject'] == @subject_nid && 
          item['country_code'] == @country_code
        }
        

        subject_subtopics_evaluation.each do |item|
          sid = item['subtopic']
          bars[sid]['progress'][period] = item['percentage'].round
        end
      end

      bars
    end


  end
end
