require 'pp'

module Jekyll
  module PageAnalytics
    def add_analytics_data
      @page_path = (@dir.index("#{@lang}") == 0) ? @dir[2..-1] : @dir
      @page_path = "/#{@page_path}"
      data['path'] = @page_path
      data['analytics_property_id'] = site.config['analytics_property_id']
    end
  end
end
