require_relative 'TranslationFilter.rb'
require_relative 'SubmissionTablesModule.rb'

module Jekyll
  class PrintCountryPage < Page
    include Jekyll::EvaluationDetails
    include Jekyll::EvalColor
    include Jekyll::TranslationFilter
    include Jekyll::PageAnalytics
    include Jekyll::SubmissionTables

    def initialize(site, base, dir, lang, country)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      @lang = lang
      @country_code = country['code']
      @country_name = country['name']
      @subjects = getSubjectsHash
      addSubjectEvaluation

      layout = "print"
      self.read_yaml(File.join(base, '_layouts'), "#{layout}.html")
      self.data['layout'] = layout

      self.process(@name)
      
      addSubjectPrintDetails
      self.data['country'] = country
      self.data['subjects'] = @subjects
    end

    # Adds all the details (recommendations and evaluation details) for each of the subjects.
    #
    def addSubjectPrintDetails
      @subjects.each do |subject_nid, subject_data|
        @subject_nid = subject_nid
        @subject_group_id = subject_data['subject_group_id']

        self.data['comparison_chart_data'] = getComparisonChartData
        
        # Add subject recommendations
        tables = getSubmissionTables(@country_code, subject_nid) 
        recommendations = tables['recommendations']
        @subjects[subject_nid]['recommendations'] = recommendations

        # Add subtopics and evaluation details (questions)
        @subjects[subject_nid]['evaluation_details'] = getEvaluationDetails(subject_nid, site.config['current_evaluation_period'])
      end
    end
  end

  class PrintCountryGenerator < Generator
    include Jekyll::GeneratorEnabler
    include Jekyll::EvaluationDetails

    def generate(site)
      return unless is_enabled? site

      puts "** Generating Print Country pages"

      # Iterate over languages
      site.config['languages'].each do |lang|

        #Iterate over countries
        countries = site.data["countries#{lang}"]
        countries.each do |country|
          if lang != site.config['default_language']
            dir = "#{lang}/countries/#{country['code']}/print"
          else
            dir = "countries/#{country['code']}/print"
          end
          site.pages << PrintCountryPage.new(site, site.source, dir, lang, country)
        end
      end
    end
  end
end
